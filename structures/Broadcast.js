/**
 * Broadcast obj structure
 */
module.exports = class Broadcast {
    /**
     * Default Broadcast constructor
     * @constructor
     * @param name {string} - name of broadcast
     * @param message {string} - message being output by broadcast
     * @param channel {Discord.Channel} - channel broadcast is being output too
     * @param delay {number} - time between broadcasts (in minutes)
     */
    constructor({ name, message, channel, delay }) {
        this.name = name;
        this.message = message;
        this.channel = channel;
        this.delay = delay;
    }
};
