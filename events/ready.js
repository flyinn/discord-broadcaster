const Broadcast = require('../structures/Broadcast');

module.exports = (client) => {
    console.log('discord-broadcaster: ready');

    client.guilds.forEach(guild => {
        const settings = client.settings.ensure(guild.id, client.defaultSettings);
        const broadcasts = client.settings.get(guild.id, 'broadcasts');

        broadcasts.forEach((broadcast) => {
            const channel = guild.channels.find(channel => channel.id === broadcast.channel);

            const interval = setInterval(() => {
                channel.send(`${broadcast.message}`);
            }, broadcast.delay);

            client.intervals.set(broadcast.name, interval);
        });
    });
};
