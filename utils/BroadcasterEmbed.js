const { RichEmbed } = require('discord.js');

/**
 * Collection of RichEmbed helper functions
 */
module.exports = class BroadcasterEmbed extends RichEmbed {
    /**
     * Inherits constructor from Discord.RichEmbed
     * @constructor
     */
    constructor(data = {}) {
        super(data);
    }

    /**
     * An err RichEmbed with prefix added
     * @function
     * @param {string} message - Error message
     *
     * @todo add color, version of red?
     */
    static err(message) {
        return new BroadcasterEmbed()
            .setDescription(`*Error!* ${message}.`);
    }

    /**
     * An notice RichEmbed with prefix added
     * @function
     * @param {string} message - Notice message
     *
     * @todo add color, version of yellow?
     */
    static notice(message) {
        return new BroadcasterEmbed()
            .setDescription(`*Notice!* ${message}.`);
    }

    /**
     * An success RichEmbed with prefix added
     * @function
     * @param {string} message - Success message
     *
     * @todo add color, version of green?
     */
    static success(message) {
        return new BroadcasterEmbed()
            .setDescription(`*Success!* ${message}.`);
    }

    /**
     * A perm denied RichEmbed
     * @function
     *
     * @todo add color
     */
    static denied() {
        return new BroadcasterEmbed()
            .setDescription('*Denied!* You do not have permission to execute this command.');
    }
};
